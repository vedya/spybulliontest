﻿namespace SPYBullionTest
{
    partial class FrmHome
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHome));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.registrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.customerRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseEntrieToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockEntrieToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.salesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.orderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saleBillsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.saleCancellationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billsCancellationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.purchaseOrderReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.salesOrderReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.stockReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billingReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientReportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.supplierReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemListToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.receiptsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.billsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deliveryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.summaryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dueReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.advanceRecieptsReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administratorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.itemDescriptionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userRegistrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxPercentageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.penaltyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taxExceptionAmountToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.registrationToolStripMenuItem,
            this.toolStripMenuItem1,
            this.salesToolStripMenuItem,
            this.toolStripMenuItem2,
            this.reportsToolStripMenuItem,
            this.administratorToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(1274, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // registrationToolStripMenuItem
            // 
            this.registrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.supplierRegistrationToolStripMenuItem,
            this.customerRegistrationToolStripMenuItem});
            this.registrationToolStripMenuItem.Name = "registrationToolStripMenuItem";
            this.registrationToolStripMenuItem.Size = new System.Drawing.Size(101, 24);
            this.registrationToolStripMenuItem.Text = "Registration";
            this.registrationToolStripMenuItem.Click += new System.EventHandler(this.registrationToolStripMenuItem_Click);
            // 
            // supplierRegistrationToolStripMenuItem
            // 
            this.supplierRegistrationToolStripMenuItem.Name = "supplierRegistrationToolStripMenuItem";
            this.supplierRegistrationToolStripMenuItem.Size = new System.Drawing.Size(221, 24);
            this.supplierRegistrationToolStripMenuItem.Text = "Supplier Registration";
            this.supplierRegistrationToolStripMenuItem.Click += new System.EventHandler(this.supplierRegistrationToolStripMenuItem_Click);
            // 
            // customerRegistrationToolStripMenuItem
            // 
            this.customerRegistrationToolStripMenuItem.Name = "customerRegistrationToolStripMenuItem";
            this.customerRegistrationToolStripMenuItem.Size = new System.Drawing.Size(221, 24);
            this.customerRegistrationToolStripMenuItem.Text = "Customer registration";
            this.customerRegistrationToolStripMenuItem.Click += new System.EventHandler(this.customerRegistrationToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseEntrieToolStripMenuItem1,
            this.stockEntrieToolStripMenuItem1});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(79, 24);
            this.toolStripMenuItem1.Text = "Purchase";
            // 
            // purchaseEntrieToolStripMenuItem1
            // 
            this.purchaseEntrieToolStripMenuItem1.Name = "purchaseEntrieToolStripMenuItem1";
            this.purchaseEntrieToolStripMenuItem1.Size = new System.Drawing.Size(178, 24);
            this.purchaseEntrieToolStripMenuItem1.Text = "Purchase Order";
            this.purchaseEntrieToolStripMenuItem1.Click += new System.EventHandler(this.purchaseEntrieToolStripMenuItem1_Click);
            // 
            // stockEntrieToolStripMenuItem1
            // 
            this.stockEntrieToolStripMenuItem1.Name = "stockEntrieToolStripMenuItem1";
            this.stockEntrieToolStripMenuItem1.Size = new System.Drawing.Size(178, 24);
            this.stockEntrieToolStripMenuItem1.Text = "Stock Entries";
            this.stockEntrieToolStripMenuItem1.Click += new System.EventHandler(this.stockEntrieToolStripMenuItem1_Click);
            // 
            // salesToolStripMenuItem
            // 
            this.salesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.orderToolStripMenuItem,
            this.deliveryToolStripMenuItem,
            this.saleBillsToolStripMenuItem});
            this.salesToolStripMenuItem.Name = "salesToolStripMenuItem";
            this.salesToolStripMenuItem.Size = new System.Drawing.Size(55, 24);
            this.salesToolStripMenuItem.Text = "Sales";
            // 
            // orderToolStripMenuItem
            // 
            this.orderToolStripMenuItem.Name = "orderToolStripMenuItem";
            this.orderToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.orderToolStripMenuItem.Text = "Sale Order";
            this.orderToolStripMenuItem.Click += new System.EventHandler(this.orderToolStripMenuItem_Click);
            // 
            // deliveryToolStripMenuItem
            // 
            this.deliveryToolStripMenuItem.Name = "deliveryToolStripMenuItem";
            this.deliveryToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.deliveryToolStripMenuItem.Text = "Delivery";
            this.deliveryToolStripMenuItem.Click += new System.EventHandler(this.deliveryToolStripMenuItem_Click);
            // 
            // saleBillsToolStripMenuItem
            // 
            this.saleBillsToolStripMenuItem.Name = "saleBillsToolStripMenuItem";
            this.saleBillsToolStripMenuItem.Size = new System.Drawing.Size(148, 24);
            this.saleBillsToolStripMenuItem.Text = "Sale Bills";
            this.saleBillsToolStripMenuItem.Click += new System.EventHandler(this.saleBillsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saleCancellationToolStripMenuItem,
            this.billsCancellationToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(109, 24);
            this.toolStripMenuItem2.Text = "Cancellations";
            // 
            // saleCancellationToolStripMenuItem
            // 
            this.saleCancellationToolStripMenuItem.Name = "saleCancellationToolStripMenuItem";
            this.saleCancellationToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.saleCancellationToolStripMenuItem.Text = "Sales Cancellation";
            // 
            // billsCancellationToolStripMenuItem
            // 
            this.billsCancellationToolStripMenuItem.Name = "billsCancellationToolStripMenuItem";
            this.billsCancellationToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.billsCancellationToolStripMenuItem.Text = "Bills Cancellation";
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.purchaseOrderReportToolStripMenuItem,
            this.salesOrderReportsToolStripMenuItem,
            this.toolStripMenuItem3,
            this.stockReportsToolStripMenuItem,
            this.billingReportsToolStripMenuItem,
            this.clientReportsToolStripMenuItem,
            this.supplierReportToolStripMenuItem,
            this.itemListToolStripMenuItem,
            this.deliveryReportToolStripMenuItem,
            this.summaryReportToolStripMenuItem,
            this.dueReportToolStripMenuItem,
            this.advanceRecieptsReportToolStripMenuItem});
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // purchaseOrderReportToolStripMenuItem
            // 
            this.purchaseOrderReportToolStripMenuItem.Name = "purchaseOrderReportToolStripMenuItem";
            this.purchaseOrderReportToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.purchaseOrderReportToolStripMenuItem.Text = "Purchase Order Report";
            // 
            // salesOrderReportsToolStripMenuItem
            // 
            this.salesOrderReportsToolStripMenuItem.Name = "salesOrderReportsToolStripMenuItem";
            this.salesOrderReportsToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.salesOrderReportsToolStripMenuItem.Text = "Sales Reports";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(244, 24);
            this.toolStripMenuItem3.Text = "Day Report";
            // 
            // stockReportsToolStripMenuItem
            // 
            this.stockReportsToolStripMenuItem.Name = "stockReportsToolStripMenuItem";
            this.stockReportsToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.stockReportsToolStripMenuItem.Text = "Stock Reports";
            // 
            // billingReportsToolStripMenuItem
            // 
            this.billingReportsToolStripMenuItem.Name = "billingReportsToolStripMenuItem";
            this.billingReportsToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.billingReportsToolStripMenuItem.Text = "Billing Reports";
            // 
            // clientReportsToolStripMenuItem
            // 
            this.clientReportsToolStripMenuItem.Name = "clientReportsToolStripMenuItem";
            this.clientReportsToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.clientReportsToolStripMenuItem.Text = "Client Reports";
            // 
            // supplierReportToolStripMenuItem
            // 
            this.supplierReportToolStripMenuItem.Name = "supplierReportToolStripMenuItem";
            this.supplierReportToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.supplierReportToolStripMenuItem.Text = "Supplier Report";
            // 
            // itemListToolStripMenuItem
            // 
            this.itemListToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.receiptsToolStripMenuItem,
            this.billsToolStripMenuItem});
            this.itemListToolStripMenuItem.Name = "itemListToolStripMenuItem";
            this.itemListToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.itemListToolStripMenuItem.Text = "Duplicate";
            // 
            // receiptsToolStripMenuItem
            // 
            this.receiptsToolStripMenuItem.Name = "receiptsToolStripMenuItem";
            this.receiptsToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.receiptsToolStripMenuItem.Text = "Receipts";
            // 
            // billsToolStripMenuItem
            // 
            this.billsToolStripMenuItem.Name = "billsToolStripMenuItem";
            this.billsToolStripMenuItem.Size = new System.Drawing.Size(134, 24);
            this.billsToolStripMenuItem.Text = "Bills";
            // 
            // deliveryReportToolStripMenuItem
            // 
            this.deliveryReportToolStripMenuItem.Name = "deliveryReportToolStripMenuItem";
            this.deliveryReportToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.deliveryReportToolStripMenuItem.Text = "Delivery Report ";
            // 
            // summaryReportToolStripMenuItem
            // 
            this.summaryReportToolStripMenuItem.Name = "summaryReportToolStripMenuItem";
            this.summaryReportToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.summaryReportToolStripMenuItem.Text = "Summary Report";
            // 
            // dueReportToolStripMenuItem
            // 
            this.dueReportToolStripMenuItem.Name = "dueReportToolStripMenuItem";
            this.dueReportToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.dueReportToolStripMenuItem.Text = "Bank Statement";
            // 
            // advanceRecieptsReportToolStripMenuItem
            // 
            this.advanceRecieptsReportToolStripMenuItem.Name = "advanceRecieptsReportToolStripMenuItem";
            this.advanceRecieptsReportToolStripMenuItem.Size = new System.Drawing.Size(244, 24);
            this.advanceRecieptsReportToolStripMenuItem.Text = "Advance Reciepts Report";
            // 
            // administratorToolStripMenuItem
            // 
            this.administratorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.itemDescriptionToolStripMenuItem,
            this.userRegistrationToolStripMenuItem,
            this.taxPercentageToolStripMenuItem,
            this.penaltyToolStripMenuItem,
            this.taxExceptionAmountToolStripMenuItem});
            this.administratorToolStripMenuItem.Name = "administratorToolStripMenuItem";
            this.administratorToolStripMenuItem.Size = new System.Drawing.Size(112, 24);
            this.administratorToolStripMenuItem.Text = "Administrator";
            // 
            // itemDescriptionToolStripMenuItem
            // 
            this.itemDescriptionToolStripMenuItem.Name = "itemDescriptionToolStripMenuItem";
            this.itemDescriptionToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.itemDescriptionToolStripMenuItem.Text = "Item Description";
            // 
            // userRegistrationToolStripMenuItem
            // 
            this.userRegistrationToolStripMenuItem.Name = "userRegistrationToolStripMenuItem";
            this.userRegistrationToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.userRegistrationToolStripMenuItem.Text = "User Registration ";
            // 
            // taxPercentageToolStripMenuItem
            // 
            this.taxPercentageToolStripMenuItem.Name = "taxPercentageToolStripMenuItem";
            this.taxPercentageToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.taxPercentageToolStripMenuItem.Text = "VAT Percentage";
            // 
            // penaltyToolStripMenuItem
            // 
            this.penaltyToolStripMenuItem.Name = "penaltyToolStripMenuItem";
            this.penaltyToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.penaltyToolStripMenuItem.Text = "Penalty";
            // 
            // taxExceptionAmountToolStripMenuItem
            // 
            this.taxExceptionAmountToolStripMenuItem.Name = "taxExceptionAmountToolStripMenuItem";
            this.taxExceptionAmountToolStripMenuItem.Size = new System.Drawing.Size(227, 24);
            this.taxExceptionAmountToolStripMenuItem.Text = "Tax Exception Amount";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.panel3);
            this.groupBox4.Location = new System.Drawing.Point(16, 44);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox4.Size = new System.Drawing.Size(680, 329);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Purchases";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.dataGridView6);
            this.panel3.Location = new System.Drawing.Point(8, 18);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(657, 303);
            this.panel3.TabIndex = 6;
            // 
            // dataGridView6
            // 
            this.dataGridView6.AllowUserToAddRows = false;
            this.dataGridView6.AllowUserToDeleteRows = false;
            this.dataGridView6.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView6.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView6.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.Column7,
            this.Column8});
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView6.DefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView6.Location = new System.Drawing.Point(1, 10);
            this.dataGridView6.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.RowHeadersVisible = false;
            this.dataGridView6.RowTemplate.Height = 24;
            this.dataGridView6.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView6.Size = new System.Drawing.Size(647, 289);
            this.dataGridView6.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Item";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Average unit price";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column8
            // 
            this.Column8.HeaderText = "Total amount";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.panel4);
            this.groupBox5.Location = new System.Drawing.Point(787, 44);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox5.Size = new System.Drawing.Size(664, 329);
            this.groupBox5.TabIndex = 21;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Sales";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.dataGridView7);
            this.panel4.Location = new System.Drawing.Point(8, 18);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(647, 303);
            this.panel4.TabIndex = 6;
            // 
            // dataGridView7
            // 
            this.dataGridView7.AllowUserToAddRows = false;
            this.dataGridView7.AllowUserToDeleteRows = false;
            this.dataGridView7.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView7.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView7.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView7.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView7.Location = new System.Drawing.Point(1, 10);
            this.dataGridView7.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.RowHeadersVisible = false;
            this.dataGridView7.RowTemplate.Height = 24;
            this.dataGridView7.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView7.Size = new System.Drawing.Size(636, 289);
            this.dataGridView7.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Item";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Average unit price";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Total amount";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Mistral", 100F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Green;
            this.label1.Location = new System.Drawing.Point(36, 396);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1214, 199);
            this.label1.TabIndex = 27;
            this.label1.Text = "Billing Utility";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.FrmHome_Load);
            this.label1.MouseEnter += new System.EventHandler(this.label1_MouseEnter);
            this.label1.MouseLeave += new System.EventHandler(this.label1_MouseLeave);
            // 
            // FrmHome
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Silver;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1274, 735);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FrmHome";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Bullion Market";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmHome_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem registrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem customerRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem orderToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem purchaseOrderReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem salesOrderReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stockReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billingReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clientReportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem supplierReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemListToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dueReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deliveryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem summaryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administratorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userRegistrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxPercentageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem penaltyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taxExceptionAmountToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem purchaseEntrieToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem stockEntrieToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem receiptsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem advanceRecieptsReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem itemDescriptionToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem saleCancellationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem billsCancellationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saleBillsToolStripMenuItem;
    }
}

