﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Test
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        public void Form2_Load(object sender, EventArgs e)
        {
        }

        private void Form2_Paint(object sender, PaintEventArgs e)
        {
            // Create four Pen objects with red,
            // blue, green, and black colors and
            // different widths
            Pen redPen = new Pen(Color.Red, 1);
            Pen bluePen = new Pen(Color.Blue, 2);
            Pen greenPen = new Pen(Color.Green, 3);
            Pen blackPen = new Pen(Color.Black, 4);

            // Draw line using float coordinates
            int i = 0;
            float x1 = 0.0F, y1 = 10.0F;
            float x2 = 20.0F, y2 = 20.0F;
            for (i = 0; i < 10; i++)
            {
                e.Graphics.DrawLine(redPen, x1, y1, x2, y2);
                x1 = x2; y1 = y2;
                x2 = x2 + 3.0F;
                y2 = y2 + 3.5F;
            }
        }
        public void paint(object sender, PaintEventArgs e)
        {
           
        }

        public void timer1_Tick(object sender, EventArgs e)
        {
             
        }

    }
}
