﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Test
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

     public void SineCurve()
     {
          Text = "Sine Curve";
          ResizeRedraw = true;
     }
     protected override void OnPaint(PaintEventArgs pea)
     {
          DoPage(pea.Graphics, ForeColor,ClientSize.Width, ClientSize.Height);
     }
     protected void DoPage(Graphics grfx, Color clr, int cx, int cy)
     {
          PointF[] aptf = new PointF[cx];
   
          for (int i = 0; i < cx; i++)
          {
               aptf[i].X = i;
               aptf[i].Y = cy / 2 * (1 -(float) 
                                   Math.Sin(i * 2 * Math.PI / (cx - 1)));
          }
          grfx.DrawLines(new Pen(clr), aptf);
}










    }
}
